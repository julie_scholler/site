+++
title = "L1 Statistiques Descriptives"
date = 2019-03-20T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: cours de licence
#  2: cours de master
#  3: Manuscrit
#  4: Technical report
#  5: Livre
#  6: Tutoriel
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Statistiques descriptives univariées et bivariées"

summary = "Statistiques descriptives univariées et bivariées"

# Caption (optional)
caption = ""

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"
  
# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","stats","licence","L1"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Polycopié", url = "L1-SD-Poly.pdf"}, 
{name = "Exercices", url = "L1-SD-fasc-exos-etu.pdf"}, 
{name = "CM-Diapos", url = "L1-SD-CM-1819.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

# share
share = false
+++

## Objectifs

* Savoir réaliser et comprendre les tableaux d-effectifs, de
fréquences, de contingence, les graphiques courants et les
données synthétiques utilisés pour résumer des données
* Développer l’esprit critique vis-à-vis des données numériques

## Contenu de l’enseignement
* Statistiques descriptives unidimensionnelles
tableaux synthétiques, graphiques, indicateurs de tendance
centrale, de dispersion, courbe de concentration, indice de Gini
* Statistiques descriptives bidimensionnelles
tableaux de contingence, liens entre variables, variances
expliquées et résiduelles, régression linéaire simple

