+++
title = "M1 Data Mining"
date = 2019-02-20T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: cours de licence
#  2: cours de master
#  3: Manuscrit
#  4: Technical report
#  5: Livre
#  6: Tutoriel
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Classification supervisée"

summary = "Classification supervisée"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"
  
# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["mecen","AD","master"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Diaporama", url = "M1-Datamining-CM-Partie2.pdf"}, 
{name = "TP", url = "M1-Datamining-TP-Partie2.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

## Objectifs

L’objectif de cet enseignement est la découverte et la mise en pratique de différentes méthodes de classification supervisée (la régression logistique n'est pas abordée ici).

## Plan du cours

* Analyse factorielle discriminante (linéaire et quadratique)
* Courbe ROC
* Estimation de l’erreur de prédiction (validation croisée, par échantillons bootstrap)
* Arbres de décision
* Agrégation de modèles (bagging, random forest, boosting)
* Gestion de données manquantes

## Organisation

Je partage ce cours avec Franck Piller. Je suis responsable de la deuxième partie : Estimation de l'erreur de prédiction, arbres de décision, agrégation de modèles et gestion des données manquantes.

