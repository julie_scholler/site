+++
title = "L2 Mathématiques pour l'économiste 4"
date = 2018-11-20T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: cours de licence
#  2: cours de master
#  3: Manuscrit
#  4: Technical report
#  5: Livre
#  6: Tutoriel
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Suites récurrentes et équaitons différentielles"

summary = "Suites récurrentes et équaitons différentielles"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"
  
# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","maths","licence","L2"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Polycopié", url = "L2-M4-Poly.pdf"}, 
{name = "Exercices", url = "L2-M4-fasc-exos-etu.pdf"}, 
{name = "CM-Diapos", url = "L2-M4-CM.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

## Objectifs

L’objectif de cet enseignement est d’acquérir les techniques de résolution d’équations dont les inconnues ne sont plus
des nombres mais des suites ou des fonctions. Une attention particulière sera portée sur des situations faisant intervenir
des paramètres.

## Contenu 

Modèles dynamiques utiles pour la modélisation d’évolution de
situation économique

* Suites récurrentes
  - linéaires à coefficients constants d’ordre 1
  - d’ordre 1 quelconques
  - linéaires à coefficients constants d’ordre 2
* Équations différentielles
  - linéaires à coefficients constants d’ordre 1
  - d’ordre 1 quelconques
  - linéaires à coefficients constants d’ordre 2

