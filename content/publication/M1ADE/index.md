+++
title = "M1 Analyse de données exploratoire"
date = 2019-03-20T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: cours de licence
#  2: cours de master
#  3: Manuscrit
#  4: Technical report
#  5: Livre
#  6: Tutoriel
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Analyse factorielle et classification non supervisée"

summary = "Analyse factorielle et classification non supervisée"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"
  
# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["mecen","AD","master"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Diaporama", url = "M1-AnalyseExploratoire.pdf"}, 
{name = "TP-ACM", target="_blank", url = "files/M1-S1-TP.html"},
{name = "Données pour TP", url = "employes.csv"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

## Objectifs

Cet enseignement est l’occasion de découvrir des méthodes avancées d’exploration de données multidimensionnelles :
analyse factorielle et classification non supervisée. Ces méthodes seront mise en pratique avec le logiciel R.

## Plan du cours

* Analyse en composantes principales
* Analyse factorielle des correspondances
* Analyse en composantes multiples
* Classification non supervisée : classification hiérarchique ascendante, méthodes des moyennes mobiles

## Organisation

Je partage ce cours avec Franck Piller. Je suis responsable de la deuxième partie : ACM et classification non supervisée.
