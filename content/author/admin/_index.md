---
authors:
- admin
bio: Je suis professeure agrégée de Mathématiques, docteure en Probabilités. J’enseigne les Mathématiques, la Statistique Inférentielle et l’Analyse de Données (DataMining, Machine Learning) à l’université de Tours, principalement en MÉcEn (Master Économiste d’entreprise) et en licence d’Économie.
education:
  courses:
  - course: Doctorat en Mathématiques/Probabilités
    institution: Université de Lorraine
    year: 2013
  - course: Master Recherche en Mathématiques
    institution: Université de Nancy 1
    year: 2009
  - course: Agrégation de Mathématiques option Probabilités
    institution:  
    year: 2008
email: ""
interests:
- Mathématiques
- Statistique inférentielle
- Analyse de Données
- R
name: Julie Scholler
organizations:
- name: Université de Tours
  url: "https://www.univ-tours.fr/site-de-l-universite/accueil--598731.kjsp"
role: Enseignante en Statistique et Mathématiques
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto: julie.scholler@univ-tours.fr'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/Pr_JScholler
- icon: cv
  icon_pack: ai
  link: "files/cv-julie_scholler.pdf"
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
#- icon: github
#  icon_pack: fab
#  link: https://github.com/gcushen
superuser: true
user_groups:
- Researchers
- Visitors
---

Je suis professeure agrégée de mathématiques, docteure en probabilités.

J’enseigne les mathématiques, la statistique inférentielle et l’analyse de données (dataMining, machine learning) à l’université de Tours, en MÉcEn (Master Économiste d’entreprise) et en licence d’Économie. 
